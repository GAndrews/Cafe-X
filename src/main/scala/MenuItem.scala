trait MenuItem
{
  def name : String
  def temperature : String
  def cost : Int
}

case class Drink(val name : String, val temperature :String, val cost : Int) extends MenuItem
case class Food(val name : String, val temperature :String, val cost : Int) extends MenuItem


