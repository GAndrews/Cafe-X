import scala.collection.mutable
import scala.collection.mutable.Set

class Menu {
  val items : Map[String, MenuItem] = Map(
    "cola" -> Drink("Cola","cold",50),
    "coffee" -> Drink("Coffee","hot",100),
    "cheese sandwich" -> Food("Cheese Sandwich","cold",200),
    "steak sandwich" -> Food("Steak Sandwich","hot",450)
  )

  def totalCost(order : String*) : String = {
      totalCost( order, 0, Set.empty[String])
  }

  private def totalCost(order : Seq[String], total : Int, tags : Set[String]) : String = {
    if (order.size == 0) {
      processServiceCharge(total, tags)
    } else {
      items.get(order(0).toLowerCase) match {
        case Some(value) =>
          totalCost(order.drop(1), total+value.cost,
            tags += s"${value.temperature}:${value.getClass.getCanonicalName}"
          )
        case None =>
          totalCost( order.drop(1), total, tags)
      }
    }
  }

  private def processServiceCharge(total: Int, tags: Set[String]) = {
    val hotFoodPattern = ".*hot:Food.*".r
    val allFoodPattern = ".*Food.*".r

    val testVal = f"${
      tags.mkString("") match {
        case hotFoodPattern() => {
          total.toFloat/100*1.2
        }
        case allFoodPattern() => {
          total.toFloat/100*1.1
        }
        case _ => total.toFloat/100
      }
    }%.2f"
    testVal
  }
}
