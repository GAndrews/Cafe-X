import org.scalatest.{FlatSpec, Matchers}

class MenuTest extends FlatSpec with Matchers {

    val menu = new Menu()

    // Step 2 as this test contains no food it should not incur a service charge
    // value remains at 1.50
    "The order" should "cost £1.50 when it contains a Cola and a Coffee" in {
        menu.totalCost("cola", "coffee") shouldBe "1.50"
    }

    //Step 2 as this test now contains cold foot it should include a 10% service charge
    // value has gone from 3.00 to 3.30
    it should "cost £3.30 when it contains a Cheese Sandwich and a Coffee" in {
        menu.totalCost("Cheese Sandwich", "coffee") shouldBe "3.30"
    }

    //Step 2 as this test now contains cold foot it should include a 20% service charge
    // value has gone from 8.00 to 9.60
    it should "cost £9.60 when it contains everything on the menu" in {
        menu.totalCost(
            menu.items.keys.toSeq: _*
        ) shouldBe "9.60"
    }

    it should "cost £0.50 when the order only contains cola and several invalid items" in {
      menu.totalCost(
          "cola",
                  "diet cola",
                  "pepsi",
                  "hot dog"
      ) shouldBe "0.50"
    }

    it should "cost £3.30 when two colas and a cheese sandwich are ordered" in {
      menu.totalCost(
          "cola",
                  "cola",
                  "cheese sandwich"
      ) shouldBe "3.30"
    }
}
